public class Port {
    public static void port(){
    Buildings.name = "Port";
    Buildings.acronym = "P";
    Buildings.buildingCode = 4;
    Buildings.turnsRequiredToBuild = 1;
    Buildings.buildingIncome = 0;
    Buildings.buildingExpense = 1000;
    Buildings.buildingCost = 1000;
    Buildings.canBuildingBePlacedEverywhere = false;
    Buildings.availableBuildSites = " @ ";
    Buildings.approval = Economy.approvalOfThePeople+5;
    Buildings.disapproval = 0;
    }
}
