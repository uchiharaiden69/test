public class Lab {
    public static void lab(){
        Buildings.name = "Laboratory";
        Buildings.acronym = "LB";
        Buildings.buildingCode = 5;
        Buildings.turnsRequiredToBuild = 1;
        Buildings.buildingIncome = 0;
        Buildings.buildingExpense = 1000;
        Buildings.buildingCost = 1000;
        Buildings.canBuildingBePlacedEverywhere = true;
        Buildings.availableBuildSites = " X ";
        Buildings.approval = Economy.approvalOfThePeople + 5;
        Buildings.disapproval = 0;
    }
}
