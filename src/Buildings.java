public class Buildings {
    public static int buildingCode;
    public static String name;
    public static String acronym;
    public static int turnsRequiredToBuild;
    public static int buildingIncome;
    public static int buildingExpense;
    public static int buildingCost;
    public static boolean canBuildingBePlacedEverywhere;
    public static String availableBuildSites;
    public static int approval;
    public static int disapproval;
}
