import java.util.Scanner;

public class Game {
    public static int currentDay = 1;
    public static int numberOfDays = 1826;
    public static int remainingDays = 1826;
    public static String CHOICE = null;
    public static int turnCount = 0;
    public static int OverallExpenses;
    public static int OverallIncome;
    public static void gameLoop(String[][] gameBoard) {
        Board.renderGameBoard(gameBoard);
        System.out.println("Choose an action:");
        System.out.println("1. Build");
        System.out.println("2. Skip To Next Turn");
        System.out.println("3. Check Accounting Report");
        System.out.println("4. Legend");
        Scanner scanner = new Scanner(System.in);
        String choice = scanner.nextLine();
        if(choice.equals("1")){
            CHOICE = "1";
            System.out.println("Select the building which you want to build: ");
            gameBoard  = CreatingBuilding.build(gameBoard);
        }
        if(choice.equals("2")){
            currentDay++;

        }
        Board.renderGameBoard(gameBoard);
        gameLoop(gameBoard);
    }
    public static void main(String[] args){
        String[][] gameBoard = Board.bootstrapGameBoard();
        Board.renderGameBoard(gameBoard);
        while(currentDay < numberOfDays){
            System.out.println("Choose an action:");
            System.out.println("1. Build");
            System.out.println("2. Skip To Next Turn");
            System.out.println("3. Check Accounting Report");
            System.out.println("4. Legend");
            Scanner scanner = new Scanner(System.in);
            String choice = scanner.nextLine();
            if(choice.equals("1")){
                CHOICE = "1";
                CreatingBuilding.build(gameBoard);
                gameLoop(gameBoard);
            }
            if(choice.equals("2")){
                currentDay++;
                remainingDays--;
                gameLoop(gameBoard);
            }

        }
    }
}
