import java.util.Random;

public class Board {
    public static int GAME_BOARD_ROW_COUNT = 7;
    public static int GAME_BOARD_COL_COUNT = 12;
    public static String UNIT_TERRAIN = " . " ;
    public static String UNIT_OFFICE = " X ";
    public static String UNIT_TRADE_ISLAND = " $ ";
    public static String UNIT_TOURIST_ISLAND = " & ";
    public static String UNIT_PORT_SLOT = " @ ";
    public static String UNIT_HQ = "HQ ";
    public static String UNIT_LAB = "LB ";
    public static boolean firstExpansion = false;
    public static boolean secondExpansion = false;
    public static int touristIsland1Row;
    public static int touristIsland1Col;
    public static int touristIsland2Row;
    public static int touristIsland2Col;
    public static int touristIsland3Row;
    public static int touristIsland3Col;
    public static int touristIsland4Row;
    public static int touristIsland4Col;
    public static int tradeIsland1Row;
    public static int tradeIsland1Col;
    public static int tradeIsland2Row;
    public static int tradeIsland2Col;
    public static int tradeIsland3Row;
    public static int tradeIsland3Col;
    public static int tradeIsland4Row;
    public static int tradeIsland4Col;
    public static int Port1Row;
    public static int Port1Col;
    public static int Port2Row;
    public static int Port2Col;
    public static int Port3Row;
    public static int Port3Col;
    public static int Port4Row;
    public static int Port4Col;
    public static int Port5Row;
    public static int Port5Col;
    public static int HQRow;
    public static int HQCol;
    public static int Lab1Row;
    public static int Lab1Col;

    public static void renderGameBoard(String[][] gameBoard) {
        for(int row = 0; row < GAME_BOARD_ROW_COUNT; row++) {
            for(int col = 0; col < GAME_BOARD_COL_COUNT; col++) {
                System.out.print(" " + gameBoard[row][col]);
            }
            System.out.println();
        }
    }
    public static String[][] bootstrapGameBoard() {
        String[][] gameBoard = new String[GAME_BOARD_ROW_COUNT][GAME_BOARD_COL_COUNT];
        gameBoard  = generatePassableTerrain(gameBoard);
        gameBoard  = generateIslandBase(gameBoard);
        gameBoard  = generateTradeIsland(gameBoard);
        gameBoard  = generateTouristIsland(gameBoard);
        if(firstExpansion){
            gameBoard = firstMapExpansion(gameBoard);
            if(secondExpansion){
                gameBoard = secondMapExpansion(gameBoard);
            }
        }
      return gameBoard;
    }
    private static String[][] firstMapExpansion(String[][] gameBoard){
        String[][] newGameBoard=new String[GAME_BOARD_ROW_COUNT+2][GAME_BOARD_COL_COUNT];
        for (int row=0;row<GAME_BOARD_ROW_COUNT+2;row++){
            for (int col=0;col<GAME_BOARD_COL_COUNT;col++){
                if(row>=0 && row<7) {
                    newGameBoard[row][col] = gameBoard[row][col];
                }
                else{
                    newGameBoard[row][col]=UNIT_TERRAIN;
                }
            }
        }
        GAME_BOARD_ROW_COUNT = GAME_BOARD_ROW_COUNT + 2;
        Random port4 = new Random();
        int port4Pos = port4.nextInt(4);
        if(port4Pos == 0){
            Port4Row = (GAME_BOARD_ROW_COUNT - 2);
            Port4Col = (GAME_BOARD_COL_COUNT - 5);
            gameBoard[Port4Row][Port4Col] = UNIT_PORT_SLOT;
        }
        if(port4Pos == 1){
            Port4Row = (GAME_BOARD_ROW_COUNT - 2);
            Port4Col = (GAME_BOARD_COL_COUNT - 6);
            gameBoard[Port4Row][Port4Col] = UNIT_PORT_SLOT;
        }
        if(port4Pos == 2){
            Port4Row = (GAME_BOARD_ROW_COUNT - 2);
            Port4Col = (GAME_BOARD_COL_COUNT - 7);
            gameBoard[Port4Row][Port4Col] = UNIT_PORT_SLOT;
        }
        if(port4Pos == 3){
            Port4Row = (GAME_BOARD_ROW_COUNT - 2);
            Port4Col = (GAME_BOARD_COL_COUNT - 8);
            gameBoard[Port4Row][Port4Col] = UNIT_PORT_SLOT;
        }
        Random randomTouristIsland = new Random();
         int randomNumber = randomTouristIsland.nextInt(GAME_BOARD_COL_COUNT);
        newGameBoard[8][randomNumber]=UNIT_TOURIST_ISLAND;
        touristIsland3Row=8;
        touristIsland3Col=randomNumber;
        Random randomTradeIsland = new Random();
        int randomNumber1 = randomTradeIsland.nextInt(GAME_BOARD_COL_COUNT);
        newGameBoard[8][randomNumber1]=UNIT_TRADE_ISLAND;
        tradeIsland3Row=8;
        tradeIsland3Col=randomNumber1;
        firstExpansion=true;

        gameBoard=newGameBoard;
        return gameBoard;
    }
    private static String[][] secondMapExpansion(String[][] gameBoard){
        String[][] newGameBoard=new String[GAME_BOARD_ROW_COUNT][GAME_BOARD_COL_COUNT+2];
        for (int row=0;row<GAME_BOARD_ROW_COUNT;row++){
            for (int col=0;col<GAME_BOARD_COL_COUNT+2;col++){
                if(col>=0 && col<12) {
                    newGameBoard[row][col] = gameBoard[row][col];
                }
                else{
                    newGameBoard[row][col]=UNIT_TERRAIN;
                }
            }
        }
        GAME_BOARD_COL_COUNT = GAME_BOARD_COL_COUNT + 2;
        Random port5 = new Random();
        int port5Pos = port5.nextInt(4);
        if(port5Pos == 0){
            Port5Row = (GAME_BOARD_ROW_COUNT - 2);
            Port5Col = (GAME_BOARD_COL_COUNT - 4);
            gameBoard[Port5Row][Port5Col] = UNIT_PORT_SLOT;
        }
        if(port5Pos == 1){
            Port5Row = (GAME_BOARD_ROW_COUNT - 3);
            Port5Col = (GAME_BOARD_COL_COUNT - 4);
            gameBoard[Port5Row][Port5Col] = UNIT_PORT_SLOT;
        }
        if(port5Pos == 2){
            Port5Row = (GAME_BOARD_ROW_COUNT - 4);
            Port5Col = (GAME_BOARD_COL_COUNT - 4);
            gameBoard[Port5Row][Port5Col] = UNIT_PORT_SLOT;
        }
        if(port5Pos == 3){
            Port5Row = (GAME_BOARD_ROW_COUNT - 5);
            Port5Col = (GAME_BOARD_COL_COUNT - 4);
            gameBoard[Port5Row][Port5Col] = UNIT_PORT_SLOT;
        }
        Random randomTouristIsland = new Random();
        int randomNumber = randomTouristIsland.nextInt(GAME_BOARD_ROW_COUNT);
        newGameBoard[randomNumber][13]=UNIT_TOURIST_ISLAND;
        touristIsland4Row=randomNumber;
        touristIsland4Col=13;
        Random randomTradeIsland = new Random();
        int randomNumber1 = randomTradeIsland.nextInt(GAME_BOARD_ROW_COUNT);
        newGameBoard[randomNumber1][13]=UNIT_TRADE_ISLAND;
        tradeIsland4Row=randomNumber1;
        tradeIsland4Col=13;
        secondExpansion=true;

        gameBoard=newGameBoard;
        return gameBoard;
    }
    public static String[][] generatePassableTerrain(String[][] gameBoard) {
        for (int row = 0; row < GAME_BOARD_ROW_COUNT; row++) {
            for (int col = 0; col < GAME_BOARD_COL_COUNT; col++) {
                gameBoard[row][col] = UNIT_TERRAIN;
            }
        }
        return gameBoard;
    }

    public static String[][] generateIslandBase(String[][] gameBoard) {

        int OBSTACLE_START_INDEX_ROW  = 2;
        int OBSTACLE_END_INDEX_ROW    = 5;
        int OBSTACLE_START_INDEX_COL  = 4;
        int OBSTACLE_END_INDEX_COL    = 7;
        if(firstExpansion){
        OBSTACLE_END_INDEX_ROW = 6;
        }
        if(secondExpansion){
            OBSTACLE_END_INDEX_COL = 8;
        }


        for(int row = OBSTACLE_START_INDEX_ROW; row < OBSTACLE_END_INDEX_ROW; row++) {
            for(int col = OBSTACLE_START_INDEX_COL; col <= OBSTACLE_END_INDEX_COL; col++) {
                gameBoard[row][col] = UNIT_OFFICE;
                Port1Row = (GAME_BOARD_ROW_COUNT - 5);
                Port1Col = (GAME_BOARD_COL_COUNT - 8);
                gameBoard[Port1Row][Port1Col] = UNIT_PORT_SLOT;
                Port2Row = (GAME_BOARD_ROW_COUNT - 5);
                Port2Col = (GAME_BOARD_COL_COUNT - 5);
                gameBoard[Port2Row][Port2Col] = UNIT_PORT_SLOT;
                Port3Row = (GAME_BOARD_ROW_COUNT - 3);
                Port3Col = (GAME_BOARD_COL_COUNT - 8);
                gameBoard[Port3Row][Port3Col] = UNIT_PORT_SLOT;
            }
        }
        Random random = new Random();
        Random random1 = new Random();
        int rowPosition = random.nextInt(3);
        int colPosition = random1.nextInt(2);
        if(rowPosition == 0){
            HQRow = (GAME_BOARD_ROW_COUNT - 4);
            if(colPosition == 0){
                HQCol = (GAME_BOARD_COL_COUNT - 6);
                gameBoard[HQRow][HQCol] = UNIT_HQ;
            }
            if(colPosition == 1){
                HQCol = (GAME_BOARD_COL_COUNT - 7);
                gameBoard[HQRow][HQCol] = UNIT_HQ;
            }
        }
        if(rowPosition == 1){
            HQRow = (GAME_BOARD_ROW_COUNT - 3);
            if(colPosition == 0){
                HQCol = (GAME_BOARD_COL_COUNT - 6);
                gameBoard[HQRow][HQCol] = UNIT_HQ;
            }
            if(colPosition == 1){
                HQCol = (GAME_BOARD_COL_COUNT - 7);
                gameBoard[HQRow][HQCol] = UNIT_HQ;
            }
        }
        if(rowPosition == 2){
            HQRow = (GAME_BOARD_ROW_COUNT - 3);
            if(colPosition == 0){
                HQCol = (GAME_BOARD_COL_COUNT - 6);
                gameBoard[HQRow][HQCol] = UNIT_HQ;
            }
            if(colPosition == 1){
                HQCol = (GAME_BOARD_COL_COUNT - 7);
                gameBoard[HQRow][HQCol] = UNIT_HQ;
            }
        }
        return gameBoard;
    }
    private static String[][] generateTradeIsland(String[][] gameBoard){
        tradeIsland1Row = (GAME_BOARD_ROW_COUNT - 7);
        tradeIsland1Col = (GAME_BOARD_COL_COUNT - 12);
        gameBoard[tradeIsland1Row][tradeIsland1Col] = UNIT_TRADE_ISLAND;
        tradeIsland2Row = (GAME_BOARD_ROW_COUNT - 1);
        tradeIsland2Col = (GAME_BOARD_COL_COUNT - 1);
        gameBoard[tradeIsland2Row][tradeIsland2Col] = UNIT_TRADE_ISLAND;

    return gameBoard;
    }
    private static String[][] generateTouristIsland(String[][] gameBoard){
        touristIsland1Row = (GAME_BOARD_ROW_COUNT - 7);
        touristIsland1Col = (GAME_BOARD_COL_COUNT - 1);
        gameBoard[touristIsland1Row][touristIsland1Col] = UNIT_TOURIST_ISLAND;
        touristIsland2Row = (GAME_BOARD_ROW_COUNT - 1);
        touristIsland2Col = (GAME_BOARD_COL_COUNT - 12);
        gameBoard[touristIsland2Row][touristIsland2Col] = UNIT_TOURIST_ISLAND;

        return gameBoard;
    }
}
