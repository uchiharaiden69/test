import java.util.Scanner;

public class CreatingBuilding {
    private static String selection = null;
    public static String[][] build(String[][] gameBoard) {
        if (Game.CHOICE.equals("1")) {
            System.out.println("Select which Building you want to build: ");
            System.out.println("1. Laboratory");
            System.out.println("2. Port");
            Scanner scanner = new Scanner(System.in);
            String choice = scanner.nextLine();
            if (choice.equals("1")) {
                Lab.lab();
                System.out.println("Select Coordinates for the building.");
                System.out.println("1. Row 3, Column 5");
                Scanner first = new Scanner(System.in);
                selection = first.nextLine();
                if (selection.equals("1")) {
                    Board.Lab1Row = Board.GAME_BOARD_ROW_COUNT - 3;
                    Board.Lab1Col = Board.GAME_BOARD_COL_COUNT - 5;
                    gameBoard[Board.Lab1Row][Board.Lab1Col] = Board.UNIT_LAB;
                }
                if (selection.equals("2")) {
                    Board.Lab1Row = Board.GAME_BOARD_ROW_COUNT - 3;
                    Board.Lab1Col = Board.GAME_BOARD_COL_COUNT - 6;
                    gameBoard[Board.Lab1Row][Board.Lab1Col] = Board.UNIT_LAB;
                }
                if (selection.equals("3")) {
                    Board.Lab1Row = Board.GAME_BOARD_ROW_COUNT - 3;
                    Board.Lab1Col = Board.GAME_BOARD_COL_COUNT - 7;
                    gameBoard[Board.Lab1Row][Board.Lab1Col] = Board.UNIT_LAB;
                }
                if (selection.equals("4")) {
                    Board.Lab1Row = Board.GAME_BOARD_ROW_COUNT - 4;
                    Board.Lab1Col = Board.GAME_BOARD_COL_COUNT - 5;
                    gameBoard[Board.Lab1Row][Board.Lab1Col] = Board.UNIT_LAB;
                }
                if (selection.equals("5")) {
                    Board.Lab1Row = Board.GAME_BOARD_ROW_COUNT - 4;
                    Board.Lab1Col = Board.GAME_BOARD_COL_COUNT - 6;
                    gameBoard[Board.Lab1Row][Board.Lab1Col] = Board.UNIT_LAB;
                }
                if (selection.equals("6")) {
                    Board.Lab1Row = Board.GAME_BOARD_ROW_COUNT - 4;
                    Board.Lab1Col = Board.GAME_BOARD_COL_COUNT - 7;
                    gameBoard[Board.Lab1Row][Board.Lab1Col] = Board.UNIT_LAB;
                }
            }
        }
    return gameBoard;
    }
}
